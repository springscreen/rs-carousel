# RsCarousel

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.0
as task for the Rohde & Schwarz job interview.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.