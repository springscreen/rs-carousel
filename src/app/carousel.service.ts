import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarouselService {

  imgs = [
    {
      title: 'Messtechnik',
      src: 'test-and-measurements-corporate-image-rohde-schwarz_200_13128_640_360_31.jpg'
    },
    {
      title: 'Cybersicherheit und Netzwerke',
      src: 'bright-side-of-cybersecurity-corporate-image-rohde-schwarz_200_13980_640_360_30.jpg'
    },
    {
      title: 'Broadcast- und Medientechnik',
      src: 'broadcast-and-media-overview-rohde-schwarz_200_61628_640_360_7.jpg'
    }
  ];

  constructor() { }

  getImgs() {
    return this.imgs;
  }
}
