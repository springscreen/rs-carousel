import { Component } from '@angular/core';
import { CarouselService } from "./carousel.service";
import { defer, timer, tap, repeat, asyncScheduler } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  imgs: { title: string; src: string; }[];
  currentSlide: number = 0;
  timeInterval: number = 2000;
  slideEvent: boolean = false;

  observer = defer(() => timer(this.timeInterval)).pipe(
    repeat(),
    tap(() => this.slideEvent = false)
  );
  
  resetProgressbar() {
    asyncScheduler.schedule(() => this.slideEvent = true, 20);
  }

  constructor(private imgData: CarouselService) {
    this.imgs = this.imgData.getImgs();
    this.resetProgressbar();
  }
  ngOnInit() {
    this.observer.subscribe(() => {
      this.currentSlide = ++this.currentSlide % this.imgs.length;
      this.resetProgressbar();
    });
  }
}
